import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';


import { MdAddShoppingCart } from 'react-icons/md';
import { ProductList } from './styles';
import api from '../../services/api';
import { FormatPrice } from '../../util/format';

function Home() {

    const [ products, setProducts ] = useState([]);

    useEffect( () => {
        async function loadProducts() {
            const response = await api.get('products');

            const data = response.data.map(product => ({
                ...product,
                priceFormatted: FormatPrice(product.price),
            }));

            setProducts(data);
        }
        loadProducts();
    },[]);


    const dispatch = useDispatch();
    const HandleAddProduct = (product => {
        dispatch({
            type: 'ADD_TO_CART',
            product,
        })
    })

    return (
        <ProductList>
            { products.map(product => (
                <li key={product.id}>
                    <img src={product.image} alt="product.title" />
                    <strong>{product.title}</strong>
                    <span>{product.priceFormatted}</span>
                    
                    <button type="button" onClick={()=> HandleAddProduct(product)}>
                        <div>
                            <MdAddShoppingCart size={16} color="#fff" />
                        </div>
                        <span>ADICIONAR AO CARRINHO</span>
                    </button>
                </li>
            ))}
        </ProductList>
    )
    
}

export default connect()(Home);