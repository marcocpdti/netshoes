import React from 'react';
import tenis1 from '../../assets/gallery/tenis1.jpg';
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md';

import { Container, ProductsTable, Total } from './styles';

function Cart() {
    return (
        <Container>
            <ProductsTable>
                <thead>
                    <tr>
                        <th />
                        <th>Produto</th>
                        <th>QTD</th>
                        <th>SUBTOTAL</th>
                        <th />
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img src={tenis1} alt="tenis1" />
                        </td>
                        <td>
                            <strong>Tenis muito massa</strong>
                            <span>R$129,90</span>
                        </td>
                        <td>
                            <div>
                                <button type="button">
                                    <MdRemoveCircleOutline size={20} color="#7159c1" />
                                </button>
                                <input type="number" readOnly value={1} />
                                <button>
                                    <MdAddCircleOutline size={20} color="#7159c1" />
                                </button>
                            </div>
                        </td>
                        <td>
                            <strong>R$258,00</strong>
                        </td>
                        <td>
                            <button type="button">
                                <MdDelete size={20} color="#7159c1" />
                            </button>
                        </td>
                    </tr>
                </tbody>

            </ProductsTable>
            <footer>
                <button type="button">Finalizar pedido</button>
                <Total>
                    <span>TOTAL</span>
                    <strong>$1920,00</strong>
                </Total>
            </footer>
        </Container>
    )
}

export default Cart;